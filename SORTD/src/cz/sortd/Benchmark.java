package cz.sortd;

import cz.sandor.dipl.benchmark.BenchmarkResult;
import cz.sandor.dipl.benchmark.IBenchmarkRunFlowController;
import cz.sandor.dipl.benchmark.MeasurableBenchmark;
import logic.SORTD;
import org.pmw.tinylog.Logger;

public class Benchmark implements MeasurableBenchmark {

    private SORTD benchmark = null;
    private IBenchmarkRunFlowController runFlowController;

    @Override
    public BenchmarkResult init(String[] strings) {
        benchmark = new SORTD();
        try {
            benchmark.init();
        } catch (Exception e) {
            Logger.error(e, "Failed to initialize.");
            return BenchmarkResult.INIT_FAILED;
        }

        if (runFlowController.isForceStopped()) {
            return BenchmarkResult.FORCE_STOPPED;
        } else {
            return BenchmarkResult.INIT_FINISHED;
        }
    }

    @Override
    public BenchmarkResult iterateAndMeasure() {
        try {
            while (runFlowController.canRunAgain()) {
                runFlowController.iterationStarted();
                benchmark.run();
                runFlowController.iterationStopped();
                runFlowController.iterationCompleted();
            }
        } catch (Exception e) {
            if (runFlowController.isIterationRunning()) {
                runFlowController.iterationFailed();
            }
            return BenchmarkResult.RUN_FAILED;
        }

        if (runFlowController.isForceStopped()) {
            return BenchmarkResult.FORCE_STOPPED;
        } else {
            return BenchmarkResult.RUN_FINISHED;
        }
    }

    @Override
    public BenchmarkResult cleanup() {
        try {
            benchmark.cleanup();
        } catch (Exception e) {
            Logger.error(e, "Cleanup failed.");
            return BenchmarkResult.CLEAN_UP_FAILED;
        }

        if (runFlowController.isForceStopped()) {
            return BenchmarkResult.FORCE_STOPPED;
        } else {
            return BenchmarkResult.CLEAN_UP_FINISHED;
        }
    }

    @Override
    public void setBenchmarkRunFlowController(IBenchmarkRunFlowController benchmarkRunFlowController) {
        this.runFlowController = benchmarkRunFlowController;
    }

    public static void main(String[] args) {
        SORTD uls = new SORTD();
        uls.init();

        for (int i = 0; i < 20; i++) {
            uls.run();
        }
    }
}
