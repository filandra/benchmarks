package cz.pdfd;

import cz.sandor.dipl.benchmark.BenchmarkResult;
import cz.sandor.dipl.benchmark.IBenchmarkRunFlowController;
import cz.sandor.dipl.benchmark.MeasurableBenchmark;
import logic.PdfAdvanced;
import org.pmw.tinylog.Logger;

public class Benchmark implements MeasurableBenchmark {

    private PdfAdvanced benchmark = null;
    private IBenchmarkRunFlowController runFlowController;

    @Override
    public BenchmarkResult init(String[] strings) {
        benchmark = new PdfAdvanced();
        try {
            benchmark.init();
        } catch (Exception e) {
            Logger.error(e, "Failed to initialize.");
            return BenchmarkResult.INIT_FAILED;
        }

        if (runFlowController.isForceStopped()) {
            return BenchmarkResult.FORCE_STOPPED;
        } else {
            return BenchmarkResult.INIT_FINISHED;
        }
    }

    @Override
    public BenchmarkResult iterateAndMeasure() {
        try {
            while (runFlowController.canRunAgain()) {
                runFlowController.iterationStarted();
                benchmark.run();
                runFlowController.iterationStopped();
                runFlowController.iterationCompleted();
            }
        } catch (Exception e) {
            if (runFlowController.isIterationRunning()) {
                runFlowController.iterationFailed();
            }
            return BenchmarkResult.RUN_FAILED;
        }

        if (runFlowController.isForceStopped()) {
            return BenchmarkResult.FORCE_STOPPED;
        } else {
            return BenchmarkResult.RUN_FINISHED;
        }
    }

    @Override
    public BenchmarkResult cleanup() {
        try {
            benchmark.cleanup();
        } catch (Exception e) {
            Logger.error(e, "Cleanup failed.");
            return BenchmarkResult.CLEAN_UP_FAILED;
        }

        if (runFlowController.isForceStopped()) {
            return BenchmarkResult.FORCE_STOPPED;
        } else {
            return BenchmarkResult.CLEAN_UP_FINISHED;
        }
    }

    @Override
    public void setBenchmarkRunFlowController(IBenchmarkRunFlowController benchmarkRunFlowController) {
        this.runFlowController = benchmarkRunFlowController;
    }

    public static void main(String[] args) {

        long start = System.currentTimeMillis();
        PdfAdvanced uls = new PdfAdvanced();

        uls.init();

        for (int i = 0; i < 30; i++) {
            uls.run();
        }
//
        long elapsed = System.currentTimeMillis() - start;
        System.out.println(elapsed);
    }
}
