package logic;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class PdfAdvanced {
    private static final int PDF_COUNT = 100;
    private static final int PAGE_COUNT = 20;
    private static final int TEXT_LENGTH = 40_000;
    private static final int POLYGON_COUNT = 80;

    private Random rand = new Random(2);
    private String directory = "data";
    private String extension = ".pdf";

    private String text;
    private PDImageXObject image;
    private PDDocument document;

    public void init() {
        File outDir = new File(directory);
        if (!outDir.exists()) {
            outDir.mkdirs();
        }
    }

    public void run() {
        long startTime = System.currentTimeMillis();

        for (int i = 0; i < PDF_COUNT; i++) {
            generatePdf(i);
        }

        long elapsedTime = System.currentTimeMillis() - startTime;
        System.out.println("Total time: " + elapsedTime);
    }

    private void deleteRecursively(File file) throws IOException {
        if (file.isDirectory()) {
            File[] entries = file.listFiles();
            if (entries != null) {
                for (File entry : entries) {
                    deleteRecursively(entry);
                }
            }
        }
        if (!file.delete()) {
            throw new IOException("Failed to delete " + file);
        }
    }

    public void cleanup() throws IOException {
        File file = new File(directory);
        deleteRecursively(file);
    }

    private void generatePdf(int number) {
//        PDDocument document = new PDDocument();

        // initialization
        try {
            initExistingDocument();
            initText();
//            initImage(document);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // create new PDF containing 100 pages
        for (int i = 0; i < PAGE_COUNT; i++) {
            PDPage page = new PDPage();
            document.addPage(page);

            try {
                PDPageContentStream contentStream = new PDPageContentStream(document, page);

                addText(contentStream);
//                addImage(contentStream);
                addPolygons(contentStream);

                contentStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // save the new PDF
        try {
            document.save(directory + File.separatorChar + number + extension);
            document.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initText() {
        if (text == null) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < TEXT_LENGTH; i++) {
                char c = (char) (65 + rand.nextInt(26));
                sb.append(c);
            }
            text = sb.toString();
        }
    }

    private void initImage(PDDocument document) throws IOException {
        image = PDImageXObject.createFromFile("/benchmark/input_resources/pdfd/matfyz_logo.png", document);
    }

    private void initExistingDocument() throws IOException {
        File file = new File("/benchmark/input_resources/pdfd/document_150.pdf");
        document = PDDocument.load(file);
    }

    private void addText(PDPageContentStream contentStream) throws IOException {
        long startTime = System.currentTimeMillis();

        PDFont font = PDType1Font.COURIER;
        initText();

        contentStream.beginText();
        contentStream.setFont(font, 12);
        contentStream.newLineAtOffset(100, 700);
        contentStream.showText(text);
        contentStream.endText();

        long elapsed = System.currentTimeMillis() - startTime;
//        System.out.print("Text " + elapsed + "; ");
    }

    private void addImage(PDPageContentStream contentStream) throws IOException {
        long startTime = System.currentTimeMillis();

        contentStream.drawImage(image, 75, 250);

        long elapsed = System.currentTimeMillis() - startTime;
//        System.out.print("Image " + elapsed + "; ");
    }

    private void addPolygons(PDPageContentStream contentStream) throws IOException {
        long startTime = System.currentTimeMillis();

        int polygon_edges = 9;
        float[] x = new float[polygon_edges];
        float[] y = new float[polygon_edges];

        for (int k = 0; k < x.length; k++) {
            x[k] = rand.nextInt(500);
            y[k] = rand.nextInt(500);
        }

        for (int i = 0; i < POLYGON_COUNT; i++) {
            for (int k = 0; k < x.length; k++) {
                x[k]++;
                y[k]++;
            }

            contentStream.drawPolygon(x, y);

            int r = rand.nextInt(255);
            int g = rand.nextInt(255);
            int b = rand.nextInt(255);

            contentStream.setStrokingColor(r, g, b);
        }

        long elapsed = System.currentTimeMillis();
//        System.out.print("Polygons: " + elapsed + "; ");
    }

    private void addPage(PDPageContentStream contentStream) throws IOException {
        long startTime = System.currentTimeMillis();

        addPolygons(contentStream);

        long elapsed = System.currentTimeMillis() - startTime;
//        System.out.print("PlusPage " + elapsed + "; ");
    }
}
