package logic;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;


public class PDFD {
    private Random rand = new Random(2);
    private String directory = "data";
    private String extension = ".pdf";

    private String text = null;

    private void initText() {
        if (text == null) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 40_000; i++) {
                char c = (char) (65 + rand.nextInt(26));
                sb.append(c);
            }
            text = sb.toString();
        }
    }

    public void run() {
        long startTime = System.currentTimeMillis();

        for (int i = 0; i < 100; i++) {
            generate_pdf(i);
//            System.out.println("Generated pdf number " + i);
        }

        long estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println(estimatedTime);
    }

    private void generate_pdf(int number) {
        PDDocument document = new PDDocument();

        // Create a new font object selecting one of the PDF base fonts
        PDFont font = PDType1Font.HELVETICA_BOLD;

        for (int i = 0; i < 100; i++) {
            PDPage page = new PDPage();
            document.addPage(page);

            try {
                // Start a new content stream which will "hold" the to be created content
                PDPageContentStream contentStream = new PDPageContentStream(document, page);
                draw_polygons(contentStream);
                // Make sure that the content stream is closed:
                contentStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            // Save the results and ensure that the document is properly closed:
//            OutputStream fos = new FileOutputStream(directory + number + extension);

            document.save(directory + File.separatorChar + number + extension);
            document.close();
        } catch (Exception e) {

        }
    }

    private void draw_polygons(PDPageContentStream contentStream) throws IOException {
        // Define a text content stream using the selected font, moving the cursor and drawing the text "Hello World"
        int polygon_edges = 9;
        float[] x = new float[polygon_edges];
        float[] y = new float[polygon_edges];

        PDFont font = PDType1Font.COURIER;

        initText();

        contentStream.beginText();
        contentStream.setFont(font, 12);
        contentStream.newLineAtOffset(100, 700);
        contentStream.showText("text");
        contentStream.endText();


        for (int k = 0; k < x.length; k++) {
            x[k] = rand.nextInt(500);
            y[k] = rand.nextInt(500);
        }

        for (int i = 0; i < 100; i++) {
            for (int k = 0; k < x.length; k++) {
                x[k]++;
                y[k]++;
            }

            contentStream.drawPolygon(x, y);

            int r = rand.nextInt(255);
            int g = rand.nextInt(255);
            int b = rand.nextInt(255);

            contentStream.setStrokingColor(r, g, b);

        }
    }
}
