package logic;

import cz.cypherd.exception.TaskFailedException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;

/**
 * Generates a random text, encrypts it and saves the secret encryption key, the encrypted text and the decrypted text
 * into a file.
 */
public class CYPHERD {
    private Random rand = new Random(2);
    private String directory = "data";
    private String extension = ".out";

    /**
     * Initializes the output directory where the output files will be stored.
     */
    public void init() {
        File outDir = new File(directory);
        if (!outDir.exists()) {
            outDir.mkdirs();
        }
    }

    /**
     * Generates the input for the encryption.
     *
     * @return The generated input.
     */
    private String generate_input() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        StringBuilder salt = new StringBuilder();
        while (salt.length() < 3000000) { // length of the random string. //4500000
            int index = rand.nextInt(SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    /**
     * Encrypts the generated input.
     *
     * @return An array containing: the secret encryption key, the encrypted text and the decrypted text, or null of an
     * exception has occurred.
     */
    private String[] encrypt() {
        try {
            String plainText = generate_input();

            SecretKey secKey = AESEncryption.getSecretEncryptionKey();
            byte[] cipherText = AESEncryption.encryptText(plainText, secKey);
            String decryptedText = AESEncryption.decryptText(cipherText, secKey);

            String[] result = {secKey.toString(), cipherText.toString(), decryptedText};
            return result;
        } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | BadPaddingException |
                IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Writes the given array into an output file.
     *
     * @param array  The array returned by the {@link #encrypt()} method.
     * @param number The sequence number of the iteration. The output filename includes the iteration number.
     * @throws IOException Thrown if failed to write the output file.
     */
    private void save(String[] array, int number) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(directory + File.separatorChar + number + extension)) {
            byte[] byteArray = Arrays.toString(array).getBytes();
            fos.write(byteArray);
        } catch (IOException e) {
            throw new IOException(e);
        }
    }

    /**
     * Runs 100 iteration of the encryption and saving the encrypted process.
     *
     * @throws TaskFailedException Thrown if an error occurred during the execution process.
     */
    public void run() throws TaskFailedException {
        try {
            long startTime = System.currentTimeMillis();

            for (int i = 0; i < 100; i++) {
                save(encrypt(), i);
            }

            long estimatedTime = System.currentTimeMillis() - startTime;
            System.out.println(estimatedTime);
        } catch (Exception e) {
            throw new TaskFailedException(e);
        }
    }

    /**
     * Recursively deletes the directories and file in the working environment.
     *
     * @param file Path to the directory where the output images are stored.
     * @throws IOException Thrown failed to delete the directory or the files.
     */
    private void deleteRecursively(File file) throws IOException {
        if (file.isDirectory()) {
            File[] entries = file.listFiles();
            if (entries != null) {
                for (File entry : entries) {
                    deleteRecursively(entry);
                }
            }
        }
        if (!file.delete()) {
            throw new IOException("Failed to delete " + file);
        }
    }

    /**
     * Deletes the generated images which contain the position of the detected human faces.
     *
     * @throws TaskFailedException Thrown if failed to delete the existing zip archive.
     */
    public void cleanup() throws TaskFailedException {
        try {
            File file = new File(directory);
            deleteRecursively(file);
        } catch (IOException e) {
            throw new TaskFailedException(e);
        }
    }
}
