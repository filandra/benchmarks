package logic;

import cz.cypherd.exception.TaskFailedException;

public class CYPHERD_Controller {

    public static void main(String[] args){
        CYPHERD cypherd = new CYPHERD();
        try {
            cypherd.run();
        } catch (TaskFailedException e) {
            e.printStackTrace();
        }
    }
}